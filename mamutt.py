#!/usr/bin/env python3
import os
import pickle
import re
import urwid

from html.parser import HTMLParser
from mastodon import Mastodon

class HTMLFilter(HTMLParser):
    """ Remove all tags from a string """
    text = ""

    def handle_endtag(self, tag):
        if tag == 'p':
            self.text += "\n\n"

    def handle_startendtag(self, tag, attrs):
        self.handle_starttag(tag, attrs)

    def handle_data(self, data):
        self.text += data

def remove_tags(string):
    """ Remove tags from string, function version """
    parser = HTMLFilter()
    parser.feed(string)
    return parser.text

def remove_emojis(string):
    """ Remove emojis from a string """
    # https://stackoverflow.com/a/58356570
    emoj = re.compile("["
        u"\U0001F600-\U0001F64F"  # emoticons
        u"\U0001F300-\U0001F5FF"  # symbols & pictographs
        u"\U0001F680-\U0001F6FF"  # transport & map symbols
        u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
        u"\U00002500-\U00002BEF"  # chinese char
        u"\U00002702-\U000027B0"
        u"\U00002702-\U000027B0"
        u"\U000024C2-\U0001F251"
        u"\U0001f926-\U0001f937"
        u"\U00010000-\U0010ffff"
        u"\u2640-\u2642" 
        u"\u2600-\u2B55"
        u"\u200d"
        u"\u23cf"
        u"\u23e9"
        u"\u231a"
        u"\ufe0f"  # dingbats
        u"\u3030"
        "]+", re.UNICODE)
    return re.sub(emoj, '', string)

class Timeline():
    """ Mastodon timeline class """
    timeline = []

    def __init__(self, access_token, api_base_url):
        """ Constructor """
        self.access_token = access_token
        self.api_base_url = api_base_url
        # Authenticate in Mastodon's instance
        self.mastodon = Mastodon(
            access_token = self.access_token,
            api_base_url = self.api_base_url
        )
        self.refresh()

    def refresh(self):
        """ Get timeline """
        self.timeline = self.mastodon.timeline()

    def toot_card(self, index):
        """ Generate the text card for a toot """
        if index == None or index >= len(self.timeline):
            return ""
        toot = self.timeline[index]
        content = ""
        author = remove_emojis(f"{toot['account']['display_name']}")
        if toot['reblog']:
            author = remove_emojis(f"{toot['reblog']['account']['display_name']} (RT by {author})")
            content = remove_tags(toot['reblog']['content'])
        else:
            content = remove_tags(toot['content'])
        content = remove_emojis(content)
        toot_card = f"From: {author}\n\n{content}\n[replies:{toot['replies_count']}  favs:{toot['favourites_count']}  reblogs:{toot['reblogs_count']}]"
        return toot_card

    def toot_items(self):
        """ Return a list of toot descriptions (author) """
        items = []
        for toot in self.timeline:
            author = remove_emojis(f"{toot['account']['display_name']}")
            if toot['reblog']:
                author = remove_emojis(f"{toot['reblog']['account']['display_name']} (RT by {author})")
            items.append(author)
        return items

class ListItem(urwid.Text):
    """ Class to display items """
    def __init__(self, caption):
        urwid.Text.__init__(self, caption)
        urwid.register_signal(self.__class__, ['activate'])

    def keypress(self, size, key):
        if key == 'enter':
            urwid.emit_signal(self, 'activate')
        else:
            return key

    def selectable(self):
        return True

class MainFrame(urwid.LineBox):
    """ Application main frame. Top half: item list / Bottom half: text """
    status_line = f"   Mamutt   | Home timeline | [R]efresh | [E]xit "

    def __init__(self, timeline):
        """ Constructor """
        self.timeline = timeline
        # Item list section
        self.items = self.timeline.toot_items()
        contents = []
        for caption in self.items:
            item = ListItem(caption)
            contents.append(urwid.AttrMap(item, 'banner', 'streak'))
        self.item_list = urwid.SimpleFocusListWalker(contents)
        urwid.connect_signal(self.item_list, 'modified', self.on_item_activated)
        list_box = urwid.AttrMap(urwid.ListBox(self.item_list), 'banner')
        # Status line
        status = urwid.Text(self.status_line)
        status_map = urwid.AttrMap(status, 'bg')
        # Text section
        self.toot_text = urwid.Text(u"")
        toot_map = urwid.AttrMap(self.toot_text, 'text')
        toot_filler = urwid.Filler(toot_map, valign='top')
        # Pile sections
        pile = urwid.Pile([list_box, toot_filler])
        layout = urwid.Frame(body=pile, footer=status_map)
        super().__init__(layout)
        self.on_item_activated()

    def refresh_list(self, items):
        """ Remove the current item list and shows a new one """
        contents = []
        urwid.disconnect_signal(self.item_list, 'modified', self.on_item_activated)
        while len(self.item_list) > 0:
            self.item_list.pop()
        self.items = items
        for caption in items:
            item = ListItem(caption)
            contents.append(urwid.AttrMap(item, 'banner', 'streak'))
        self.item_list.extend(contents)
        self.item_list.set_focus(0)
        urwid.connect_signal(self.item_list, 'modified', self.on_item_activated)

    def refresh_timeline(self):
        """ Refresh timeline """
        self.timeline.refresh()
        self.refresh_list(self.timeline.toot_items())

    def on_item_activated(self):
        """ Item changed, display toot content """
        focus_int = self.item_list.get_focus()[1]
        toot_text = self.timeline.toot_card(focus_int)
        self.toot_text.set_text(toot_text)

    def keypress(self, size, key):
        """ Main Frame input handling """
        if key in ('e', 'E'):
            raise urwid.ExitMainLoop()  # Exit
        elif key in ('r', 'R'):
            self.refresh_timeline()
        else:
            super().keypress(size, key)

# Get config from the environment
access_token = os.getenv('MASTODON_SECRET')
api_base_url = os.getenv('MASTODON_HOST')
if access_token == None or api_base_url == None:
    print("(!) Error: $MASTODON_SECRET and $MASTODON_HOST are required.")
    exit(0)

# Colors
palette = [
    ('banner', 'black', 'white'),
    ('streak', 'white', 'dark red'),
    ('text', 'light gray', 'black'),
    ('bg', 'white', 'dark blue'),
]

# Get items
timeline = Timeline(access_token, api_base_url)

# Create main display
main_frame = MainFrame(timeline)

# Run
loop = urwid.MainLoop(main_frame, palette=palette) #, unhandled_input=handle_input)
loop.run()

# mamutt

A very simple (and currently buggy) Mastodon TUI client.

## Installation

### Libraries

This program requires Python 3 and some libraries:

- Urwid
- Mastodon.py

The best way to install these dependencies is using Python's Virtual Environment.

```bash
$ python3 -m venv venv
$ source venv/bin/activate
$ pip install -r requirements.txt
```

## Environment variables

Two environment variables must be set to execute the program:

- MASTODON\_SECRET
- MASTODON\_HOST

The secret access token can be created in a Mastodon's user preferences page. The host is the instance used (e.g. "mastodon.social").


## Authors and acknowledgment

(C) 2022 Víctor R. Ruiz


## License

MIT License

